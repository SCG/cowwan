%% Run the SEIR-WW-EKF. Calibration is not required every time new data is available.

%Load the data and parameters
regionName = 'myRegion';

%Path of the data file
dataFile = ['./data/' regionName '.xlsx'];

load(['./parameters/params_' regionName '.mat'])
addpath('./SEIRWWfiles/')
TT = readtable(dataFile);
YC = TT.cases';
YW = TT.ww';

%Determine c_t and plot label dates. Remember to update the specialHolidays
%indices if needed.
[C, labs, firsts, longDates] = SEIRWWinit(YC, startDate, specialHolidays, params.darkNumber);

%% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% 
%
%   SECTIONS BELOW CAN BE RUN ONE-BY-ONE
%
%%% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% 
%% Estimate the wastewater data using only the case data
params.RW = params.RW0/10;
[Yest, ~, ~, ReffCase, ~] = SEIR_WW(params,YC,YW,C,[true,false],1000,firsts,labs,true);

%% Estimate the case numbers using only the wastewater data
params.RW = params.RW0/10;
[Yest, ~, ~, ReffWW, ~] = SEIR_WW(params,YC,YW,C,[false,true],1000,firsts,labs,true);

%% Estimate the case numbers using only the interpolated wastewater data
YWip = WWinterpol(YW);
params.RW = params.RW0/10;
Yest = SEIR_WW(params,YC,YWip,C,[false,true],1000,firsts,labs,true);

%% R_eff estimates from case data and WW data

params.RW = params.RW0/10;
[~, ~, ~, ReffCase, ~] = SEIR_WW(params,YC,YW,C,[true,false],1000,firsts,labs,false);
[~, ~, ~, ReffWW, ~] = SEIR_WW(params,YC,YW,C,[false,true],1000,firsts,labs,false);
figure('Position',[100,200,1200,450])
plot(ReffCase,':k','LineWidth',2)
hold on
grid
plot(ReffWW,'r','LineWidth',2)
plot([1 length(ReffCase)+10],[1 1],'k')
set(gca,'FontSize',14)
xticks(firsts)
xticklabels(labs)
legend({'Case data','Wastewater data'},'Location','Northeast','FontSize',14)
ylabel('R_{eff}','FontSize',16)
xlabel('Date','FontSize',16) 

%% Project forward in time

%At what date is the forward prediction done
predDay = length(YC);

%Use case/WW data or both
dataToUse = [true, false];

params.RW = params.RW0/10;
[~, XendC, PC] = SEIR_WW(params,YC,YW,C,dataToUse,predDay,firsts,labs,false);
[Y0, err] = SEIR_WW_FWD(XendC,C,PC,predDay+1,params,330);
Yc0 = cumsum([YC(1:predDay) Y0(1,:)]);
Xaux = XendC;
Xaux(7) = XendC(7) + PC(7,7).^.5;
Y1 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Yc1 = cumsum([YC(1:predDay) Y1(1,:)]);
Xaux(7) = XendC(7) + 2*PC(7,7).^.5;
Y2 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Yc2 = cumsum([YC(1:predDay) Y2(1,:)]);
Xaux(7) = XendC(7) - PC(7,7).^.5;
Ym1 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Ycm1 = cumsum([YC(1:predDay) Ym1(1,:)]);
Xaux(7) = XendC(7) - 2*PC(7,7).^.5;
Ym2 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Ycm2 = cumsum([YC(1:predDay) Ym2(1,:)]);

figure('Position',[400,200 560 380]); grid; hold on;
plot(movmean([YC(1:predDay) Y0(1,:)],[6,0]),'r','LineWidth',2)
plot(movmean(YC,[6,0]),'k','LineWidth',2)
xlabel('Date','FontSize',16)
ylabel('Daily cases','FontSize',16)
set(gca,'FontSize',14)
xticks(firsts)
xticklabels(labs)
legend({'Projection','Data'},'Location','Northwest','FontSize',14)

figure('Position',[400,200 560 380]); grid on; hold on;
plot(Yc0,'r','LineWidth',2)
plot(cumsum(YC),'k','LineWidth',2)
h1=fill([min(predDay,length(YC)):length(Yc0),fliplr(predDay:length(Yc0))],[Yc0(predDay),Ycm2(predDay+1:length(Ycm2)),fliplr(Yc2(predDay+1:length(Yc2))),Yc0(predDay)],[1,.77,.77],'EdgeColor','none');
h2=fill([min(predDay,length(YC)):length(Yc0),fliplr(predDay:length(Yc0))],[Yc0(predDay),Ycm1(predDay+1:length(Ycm1)),fliplr(Yc1(predDay+1:length(Yc1))),Yc0(predDay)],[1,.67,.67],'EdgeColor','none');
plot(Yc0,'r','LineWidth',2)
plot(cumsum(YC),'k','LineWidth',2)
set(h1,'facealpha',.3)
set(h2,'facealpha',.3)
set(gca,'layer','top');
xlabel('Date','FontSize',16)
ylabel('Cumulative cases','FontSize',16)
set(gca,'FontSize',14)
xticks(firsts)
xticklabels(labs)
legend({'Projection','Data'},'Location','Northwest','FontSize',14)






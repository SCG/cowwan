%% 

regions = {'Barcelona','Kitchener','Kranj','Lausanne','Ljubljana','Luxembourg','Milwaukee','Netherlands','Oshkosh','Raleigh','Riera_de_la_bisbal','Zurich'};
addpath('../SEIRWWfiles/')

winLengths = 1:21;

clear('caseErs','wwErs','bothErs','caseErsTot','wwErsTot','bothErsTot')

for jr = 1:length(regions)

    region = regions{jr};
    load(['../parameters/params_' region '.mat'])

    caseOld = zeros(1000,1);
    wwOld = zeros(1000,1);
    bothOld = zeros(1000,1);
    trueOld = zeros(1000,1);
    
    for jw = 1:length(winLengths)

        winLength = winLengths(jw);
        WWinds = find(YW>-.5);
        excl = sum(WWinds+winLength > length(YC));
        WWinds = WWinds(1:end-excl);
        trueCases = zeros(length(WWinds),1);
        for jd = 1:length(WWinds)
            trueCases(jd) = sum(YC(WWinds(jd)+(1:winLength)));
        end
        prediction

        
        ersCase = 1e5/params.N*(trueCases-predsCase)./trueCases.^.5;
        ersWW = 1e5/params.N*(trueCases-predsWW)./trueCases.^.5;
        ersBoth = 1e5/params.N*(trueCases-predsBoth)./trueCases.^.5;
        
        iii = 1:length(trueCases);
        iii = iii(find(trueCases > 0));
        
        caseErsTot{jr,jw} = abs(ersCase(iii));
        wwErsTot{jr,jw} = abs(ersWW(iii));
        bothErsTot{jr,jw} = abs(ersBoth(iii));
        
        
        predsCase = predsCase - caseOld(1:length(predsCase));
        predsWW = predsWW - wwOld(1:length(predsWW));
        predsBoth = predsBoth - bothOld(1:length(predsBoth));
        trueCases = trueCases - trueOld(1:length(trueCases));
        
        caseOld = predsCase;
        wwOld = predsWW;
        bothOld = predsBoth;
        trueOld = trueCases;
       
        ersCase = 1e5/params.N*(trueCases-predsCase)./trueCases.^.5;
        ersWW = 1e5/params.N*(trueCases-predsWW)./trueCases.^.5;
        ersBoth = 1e5/params.N*(trueCases-predsBoth)./trueCases.^.5;

        iii = 1:length(trueCases);
        iii = iii(find(trueCases > 0));

        
        caseErs{jr,jw} = abs(ersCase(iii));
        wwErs{jr,jw} = abs(ersWW(iii));
        bothErs{jr,jw} = abs(ersBoth(iii));
        
        caseErsMean(jr,jw) = mean(caseErs{jr,jw});
        wwErsMean(jr,jw) = mean(wwErs{jr,jw});
        bothErsMean(jr,jw) = mean(bothErs{jr,jw});
        
        caseErsTotMean(jr,jw) = mean(caseErsTot{jr,jw});
        wwErsTotMean(jr,jw) = mean(wwErsTot{jr,jw});
        bothErsTotMean(jr,jw) = mean(bothErsTot{jr,jw});
        
        
    end
    disp(['Region ' num2str(jr) ' done.'])
end
    

save('../results/winLengthResults.mat','caseErs','wwErs','bothErs','caseErsTot','wwErsTot','bothErsTot','caseErsMean','wwErsMean','bothErsMean','caseErsTotMean','wwErsTotMean','bothErsTotMean')



%% Define parameters

clear('params')
load('../parameters/params_Luxembourg.mat')
addpath('../SEIRWWfiles')   

params.RW = params.RW0/10;
params0 = params;


winLength = 7;
WWinds = find(YW>-.5);
excl = sum(WWinds+winLength > length(YC));
WWinds = WWinds(1:end-excl);
trueCases = zeros(length(WWinds),1);
for jd = 1:length(WWinds)
    trueCases(jd) = sum(YC(WWinds(jd)+(1:winLength)));
end

iii = find(trueCases>0);

%% Sensitivity against change of c_t (dark number)

params = params0;
prediction
Cer(1) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(1) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(1) = 1e5/params.N*mean(abs(ersBoth(iii)));

params.ctFactor = .9;
prediction
Cer(2) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(2) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(2) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = 1.1;
prediction
Cer(3) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(3) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(3) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = .8;
prediction
Cer(4) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(4) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(4) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = 1.2;
prediction
Cer(5) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(5) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(5) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = .7;
prediction
Cer(6) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(6) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(6) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = 1.3;
prediction
Cer(7) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(7) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(7) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = .6;
prediction
Cer(8) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(8) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(8) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = 1.4;
prediction
Cer(9) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(9) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(9) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = .5;
prediction
Cer(10) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(10) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(10) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.ctFactor = 1.5;
prediction
Cer(11) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(11) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(11) = 1e5/params.N*mean(abs(ersBoth(iii)));


plot(-50:10:50,[Cer(10:-2:2) Cer(1:2:11)],'LineWidth',2)
grid
hold on
plot(-50:10:50,[WWer(10:-2:2) WWer(1:2:11)],':','LineWidth',2)
plot(-50:10:50,[Ber(10:-2:2) Ber(1:2:11)],'-.','LineWidth',2)
set(gca,'FontSize',14)
xticks(-50:10:50)
xlabel('% Change in c_t','FontSize',16)
ylabel('Average standardised error per 100,000 inh.','FontSize',16)
legend({'Cases','Wastewater','Combined'},'Fontsize',14,'Location','Northeast')
%annotation('textbox', [.9, .065, 0, 0], 'string', '%','FontSize',16)

%% Sensitivity against change of kappa

params = params0;
prediction
Cer(1) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(1) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(1) = 1e5/params.N*mean(abs(ersBoth(iii)));

params.modelErrorC = .9^2*params0.modelErrorC;
prediction
Cer(2) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(2) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(2) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = 1.1^2*params0.modelErrorC;
prediction
Cer(3) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(3) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(3) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = .8^2*params0.modelErrorC;
prediction
Cer(4) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(4) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(4) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = 1.2^2*params0.modelErrorC;
prediction
Cer(5) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(5) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(5) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = .7^2*params0.modelErrorC;
prediction
Cer(6) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(6) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(6) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = 1.3^2*params0.modelErrorC;
prediction
Cer(7) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(7) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(7) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = .6^2*params0.modelErrorC;
prediction
Cer(8) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(8) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(8) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = 1.4^2*params0.modelErrorC;
prediction
Cer(9) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(9) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(9) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = .5^2*params0.modelErrorC;
prediction
Cer(10) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(10) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(10) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.modelErrorC = 1.5^2*params0.modelErrorC;
prediction
Cer(11) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(11) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(11) = 1e5/params.N*mean(abs(ersBoth(iii)));

figure
plot(-50:10:50,[Cer(10:-2:2) Cer(1:2:11)],'LineWidth',2)
grid
hold on
plot(-50:10:50,[WWer(10:-2:2) WWer(1:2:11)],':','LineWidth',2)
plot(-50:10:50,[Ber(10:-2:2) Ber(1:2:11)],'-.','LineWidth',2)
set(gca,'FontSize',14)
xticks(-50:10:50)
xlabel('% Change in kappa','FontSize',16)
ylabel('Average standardised error per 100,000 inh.','FontSize',16)
legend({'Cases','Wastewater','Combined'},'Fontsize',14,'Location','Northeast')

%% Sensitivity against change of q_beta

params = params0;
prediction
Cer(1) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(1) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(1) = 1e5/params.N*mean(abs(ersBoth(iii)));

params.Q_beta0 = .9*params0.Q_beta0;
params.Q_beta1 = .9*params0.Q_beta1;
prediction
Cer(2) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(2) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(2) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = 1.1*params0.Q_beta0;
params.Q_beta1 = 1.1*params0.Q_beta1;
prediction
Cer(3) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(3) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(3) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = .8*params0.Q_beta0;
params.Q_beta1 = .8*params0.Q_beta1;
prediction
Cer(4) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(4) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(4) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = 1.2*params0.Q_beta0;
params.Q_beta1 = 1.2*params0.Q_beta1;
prediction
Cer(5) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(5) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(5) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = .7*params0.Q_beta0;
params.Q_beta1 = .7*params0.Q_beta1;
prediction
Cer(6) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(6) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(6) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = 1.3*params0.Q_beta0;
params.Q_beta1 = 1.3*params0.Q_beta1;
prediction
Cer(7) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(7) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(7) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = .6*params0.Q_beta0;
params.Q_beta1 = .6*params0.Q_beta1;
prediction
Cer(8) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(8) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(8) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = 1.4*params0.Q_beta0;
params.Q_beta1 = 1.4*params0.Q_beta1;
prediction
Cer(9) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(9) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(9) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = .5*params0.Q_beta0;
params.Q_beta1 = .5*params0.Q_beta1;
prediction
Cer(10) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(10) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(10) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.Q_beta0 = 1.5*params0.Q_beta0;
params.Q_beta1 = 1.5*params0.Q_beta1;
prediction
Cer(11) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(11) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(11) = 1e5/params.N*mean(abs(ersBoth(iii)));

figure
plot(-50:10:50,[Cer(10:-2:2) Cer(1:2:11)],'LineWidth',2)
grid
hold on
plot(-50:10:50,[WWer(10:-2:2) WWer(1:2:11)],':','LineWidth',2)
plot(-50:10:50,[Ber(10:-2:2) Ber(1:2:11)],'-.','LineWidth',2)
set(gca,'FontSize',14)
xticks(-50:10:50)
xlabel('% Change in Q_{beta}','FontSize',16)
ylabel('Average standardised error per 100,000 inh.','FontSize',16)
legend({'Cases','Wastewater','Combined'},'Fontsize',14,'Location','Northeast')




%% Sensitivity against change of E(0)/I(0)/var(E(0))/var(I(0))

params = params0;
prediction
Cer(1) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(1) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(1) = 1e5/params.N*mean(abs(ersBoth(iii)));

params.E_init = .9*params0.E_init;
params.I_init = .9*params0.I_init;
params.varE_init = .9^2*params0.varE_init;
params.varI_init = .9^2*params0.varI_init;
prediction
Cer(2) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(2) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(2) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = 1.1*params0.E_init;
params.I_init = 1.1*params0.I_init;
params.varE_init = 1.1^2*params0.varE_init;
params.varI_init = 1.1^2*params0.varI_init;
prediction
Cer(3) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(3) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(3) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = .8*params0.E_init;
params.I_init = .8*params0.I_init;
params.varE_init = .8^2*params0.varE_init;
params.varI_init = .8^2*params0.varI_init;
prediction
Cer(4) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(4) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(4) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = 1.2*params0.E_init;
params.I_init = 1.2*params0.I_init;
params.varE_init = 1.2^2*params0.varE_init;
params.varI_init = 1.2^2*params0.varI_init;
prediction
Cer(5) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(5) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(5) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = .7*params0.E_init;
params.I_init = .7*params0.I_init;
params.varE_init = .7^2*params0.varE_init;
params.varI_init = .7^2*params0.varI_init;
prediction
Cer(6) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(6) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(6) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = 1.3*params0.E_init;
params.I_init = 1.3*params0.I_init;
params.varE_init = 1.3^2*params0.varE_init;
params.varI_init = 1.3^2*params0.varI_init;
prediction
Cer(7) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(7) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(7) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = .6*params0.E_init;
params.I_init = .6*params0.I_init;
params.varE_init = .6^2*params0.varE_init;
params.varI_init = .6^2*params0.varI_init;
prediction
Cer(8) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(8) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(8) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = 1.4*params0.E_init;
params.I_init = 1.4*params0.I_init;
params.varE_init = 1.4^2*params0.varE_init;
params.varI_init = 1.4^2*params0.varI_init;
prediction
Cer(9) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(9) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(9) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = .5*params0.E_init;
params.I_init = .5*params0.I_init;
params.varE_init = .5^2*params0.varE_init;
params.varI_init = .5^2*params0.varI_init;
prediction
Cer(10) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(10) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(10) = 1e5/params.N*mean(abs(ersBoth(iii)));


params.E_init = 1.5*params0.E_init;
params.I_init = 1.5*params0.I_init;
params.varE_init = 1.5^2*params0.varE_init;
params.varI_init = 1.5^2*params0.varI_init;
prediction
Cer(11) = 1e5/params.N*mean(abs(ersCase(iii)));
WWer(11) = 1e5/params.N*mean(abs(ersWW(iii)));
Ber(11) = 1e5/params.N*mean(abs(ersBoth(iii)));

figure
plot(-50:10:50,[Cer(10:-2:2) Cer(1:2:11)],'LineWidth',2)
grid
hold on
plot(-50:10:50,[WWer(10:-2:2) WWer(1:2:11)],':','LineWidth',2)
plot(-50:10:50,[Ber(10:-2:2) Ber(1:2:11)],'-.','LineWidth',2)
set(gca,'FontSize',14)
xticks(-50:10:50)
xlabel('% Change in E(0), I(0), and their SDs','FontSize',16)
ylabel('Average standardised error per 100,000 inh.','FontSize',16)
legend({'Cases','Wastewater','Combined'},'Fontsize',14,'Location','Northeast')

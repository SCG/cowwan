function [fitresult, gof] = createFit(data_adj, data_WW)

% Fit:
[xData, yData] = prepareCurveData( data_adj, data_WW );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );

end

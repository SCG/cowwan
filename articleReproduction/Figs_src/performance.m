% Prepare data for performance figure

function err = performance(parentdir,file)

load([parentdir + "/results/"+ file+ ".mat"])

err = zeros(4,1)';

trueCases(trueCases==0) = 1;

err(1) = mean(abs((trueCases-predsCase)./trueCases.^.5));
err(2) = mean(abs((trueCases-predsWW)./trueCases.^.5));
err(3) = std(abs((trueCases-predsCase)./trueCases.^.5));
err(4) = std(abs((trueCases-predsWW)./trueCases.^.5));

end
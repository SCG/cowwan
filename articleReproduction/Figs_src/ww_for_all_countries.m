%% Analysis of correlation betwee WW data and detected cases
% Is the relationship perfectly linear?
% Also: plots of measured data

% Daniele Proverbio & Francoise Kemp, LCSB, 16/07/2021

function ww_for_all_countries(parentdir)


for k=1:12
    names = ["Barcelona";"Kitchener";"Kranj";"Lausanne";"Ljubljana";"Luxembourg";"Milwaukee";"Netherlands";"Oshkosh";"Raleigh";"Riera_de_la_bisbal";"Zurich"];
    read=readtable([parentdir + "/data/"+ names(k) + ".xlsx"]);
    data = movmean(read.cases,7);
    dates = read.date;
    
    data_WW = read.ww;
    data_adj = data(data_WW>0);
    dates_WW = read.date;
    dates_WW = dates_WW(data_WW>0);
    data_WW = data_WW(data_WW>0);
    
    
    % Time series and correlations
    
    index_test = zeros(length(dates_WW),1);
    for n = 4:length(dates_WW)
        index_test(n) = find(dates == dates_WW(n));
    end
    index_test = index_test(4:end);
    d1 = data(index_test);
    d2 = data_WW(4:end);
    d3 = log10(data_WW(4:end));
    
    % Linear fit of (d1 vs d2) with Curve Fitting app (including 96% uncertainties)
    [curvepar, fitpar]=createFit(d1, d2);
    p=coeffvalues(curvepar);
    ci = confint(curvepar,0.95);
    y_err1 = ci(1,1)*d1 +ci(1,2);
    y_err2 = ci(2,1)*d1 +ci(2,2);
    
    corr_coef = corr(data(index_test), data_WW(4:end));
    
    
    figure( 'Position', [10 10 900 300])
    subplot(1,2,1)
    hold on
    yyaxis left
    plot(dates,data,'linewidth',1.5)
    xlabel('Dates')
    ylabel('Positive detected cases','fontsize',12)
    
    yyaxis right
    plot(dates_WW,data_WW,'o-','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',4,'linewidth',1.5)
    ylabel('SARS-CoV-2 RNA abundance in wastewater','fontsize',12)
    set(gca,'linewidth',1)
    
    subplot(1,2,2)
    hold on;
    plot(data(index_test), data_WW(4:end),'o')
    plot(sort(d1),sort(p(1)*d1+p(2)),'k','linestyle','-','linewidth',0.8)
    fill([sort(d1)', fliplr(sort(d1)')], [sort(y_err1)', fliplr(sort(y_err2)')], 'green','linestyle','none')
    alpha(0.1)
    xlabel('Positive detected cases')
    ylabel('SARS-CoV-2 RNA abundance in wastewater')
    txt = ['\rho = ', num2str(round(corr_coef,2)),newline,'Adj R-square = ',num2str(round(fitpar.adjrsquare,2))];
    NW = [min(xlim) max(ylim)*0.9]+[diff(xlim) -diff(ylim)]*0.05;
    text(NW(1), NW(2),txt,'fontsize',13)
    grid on
    set(gca,'linewidth',1)
end
end

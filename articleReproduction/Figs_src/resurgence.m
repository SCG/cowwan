% Database of epidemic resurgences for all considered countries, with
% indications for plotting

classdef resurgence


    
    properties
        parentdir
        
        % Prediction outputs
        file_list_preds_7      
        names1 

    end
    
    methods
        
        function obj = resurgence(parentdir,file_list_preds_7,names1)
            obj.parentdir = parentdir;
            obj.file_list_preds_7 = file_list_preds_7;
            obj.names1 = names1;
        end
        
        function [pred, pred_dates] = load(obj,k)
            pred = load([obj.parentdir + "/results/"+ obj.file_list_preds_7(k) + ".mat"]); 
            read = readtable(obj.parentdir +  "/data/" + obj.names1(k) + ".xlsx");
            pred_dates = read.date;
            pred_dates = pred_dates(read.ww > 0 );
            pred_dates = pred_dates(2:end-1);            
        end
        
        function bar1(obj)
            k=1;       
            [pred, pred_dates] = load(obj,k);
            pred_dates = pred_dates(7:end);
            where = 5;
            
            hold on
            plot(pred_dates(where:where+6),pred.trueCases(where:where+6),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsWW(where:where+6),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsCase(where:where+6),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 5000 10000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+6)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on            %title("Barcelona Prat de Llobregat, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
            box on
        end
        
        function bar2(obj)
            k=1;       
            [pred, pred_dates] = load(obj,k);
            pred_dates = pred_dates(7:end);
            where = 16;
            
            hold on
            plot(pred_dates(where:where+6),pred.trueCases(where:where+6),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsWW(where:where+6),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsCase(where:where+6),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 5000 10000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+6)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Barcelona Prat de Llobregat, Jan. 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function bar3(obj)
            k=1;       
            [pred, pred_dates] = load(obj,k);
            pred_dates = pred_dates(7:end);
            where = length(pred_dates)-7;
            
            hold on
            plot(pred_dates(where:where+6),pred.trueCases(where:where+6),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsWW(where:where+6),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsCase(where:where+6),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 9000 18000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+6)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Barcelona Prat de Llobregat, June 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function kit1(obj)
            k=2;       
            [pred, pred_dates] = load(obj,k);
            where = 35;
            
            hold on
            plot(pred_dates(where:where+14),pred.trueCases(where:where+14),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+14),pred.predsWW(where:where+14),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+14),pred.predsCase(where:where+14),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[50 175 300])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+14)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Kitchener, Apr. 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function kit2(obj)
            k=2;       
            [pred, pred_dates] = load(obj,k);
            where = 83;
            
            hold on
            plot(pred_dates(where:where+12),pred.trueCases(where:where+12),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+12),pred.predsWW(where:where+12),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+12),pred.predsCase(where:where+12),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[100 175 250])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+12)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Kitchener, June 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function kra1(obj)
            k=3;       
            [pred, pred_dates] = load(obj,k);
            where = 1;
            
            hold on
            plot(pred_dates(where:where+5),pred.trueCases(where:where+5),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+5),pred.predsWW(where:where+5),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+5),pred.predsCase(where:where+5),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[200 500 800])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+5)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Kranj, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function lau1(obj)
            k=4;       
            [pred, pred_dates] = load(obj,k);
            where = 1;
            
            hold on
            plot(pred_dates(where:where+12),pred.trueCases(where:where+12),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+12),pred.predsWW(where:where+12),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+12),pred.predsCase(where:where+12),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'YTick',[0 1950 3900])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+12)])
            xtickangle(0)
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            xlim([pred_dates(where), pred_dates(where+12)])
            %title("Lausanne, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function lju1(obj)
            k=5;       
            [pred, pred_dates] = load(obj,k);
            where = 1;
            
            hold on
            plot(pred_dates(where:where+10),pred.trueCases(where:where+10),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsWW(where:where+10),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsCase(where:where+10),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 1500 3000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+10)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Ljubljana, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function lux1(obj)
            k=6;       
            where = 1;
            [pred, pred_dates] = load(obj,k);
            
            hold on
            plot(pred_dates(where:where+6),pred.trueCases(where:where+6),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsWW(where:where+6),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsCase(where:where+6),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 500 1000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+6)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Luxembourg, Mar. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function lux2(obj)
            k=6;       
            where = 11;
            [pred, pred_dates] = load(obj,k);
            
            hold on
            plot(pred_dates(where:where+6),pred.trueCases(where:where+6),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsWW(where:where+6),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsCase(where:where+6),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'YTick',[0 500 1000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+6)])
            xtickangle(0)
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Luxembourg, July 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function lux3(obj)
            k=6;       
            where = 26;
            [pred, pred_dates] = load(obj,k);
            
            hold on
            plot(pred_dates(where:where+6),pred.trueCases(where:where+6),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsWW(where:where+6),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsCase(where:where+6),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 4000 8000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+6)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Luxembourg, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function lux4(obj)
            k=6;       
            where = 97;
            [pred, pred_dates] = load(obj,k);
            
            hold on
            plot(pred_dates(where:where+6),pred.trueCases(where:where+6),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsWW(where:where+6),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+6),pred.predsCase(where:where+6),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'YTick',[0 500 1000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+6)])
            xtickangle(0)
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Luxembourg, June 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function mil1(obj)
            k=7;       
            [pred, pred_dates] = load(obj,k);
            where = 18;
            
            hold on
            plot(pred_dates(where:where+14),pred.trueCases(where:where+14),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+14),pred.predsWW(where:where+14),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+14),pred.predsCase(where:where+14),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'YTick',[0 3000 6000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+14)])
            xtickangle(0)
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Milwaukee, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function mil2(obj)
            k=7;       
            [pred, pred_dates] = load(obj,k);
            where = length(pred_dates)-10;
            
            hold on
            plot(pred_dates(where:where+10),pred.trueCases(where:where+10),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsWW(where:where+10),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsCase(where:where+10),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 500 1000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+10)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Milwaukee, July 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function net1(obj)
            k=8;       
            [pred, pred_dates] = load(obj,k);
            where = 10;
            
            hold on
            plot(pred_dates(where:where+40),pred.trueCases(where:where+40),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+40),pred.predsWW(where:where+40),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+40),pred.predsCase(where:where+40),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'YTick',[0 50000 100000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+40)])
            xtickangle(0)
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Netherlands, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function net2(obj)
            k=8;       
            [pred, pred_dates] = load(obj,k);
            where = 80;
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            hold on
            plot(pred_dates(where:where+30),pred.trueCases(where:where+30),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+30),pred.predsWW(where:where+30),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+30),pred.predsCase(where:where+30),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[24000 64000 104000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+30)])
            grid on
            box on
            %title("Netherlands, Dec. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function net3(obj)
            k=8;       
            [pred, pred_dates] = load(obj,k);
            where = length(pred_dates)-28;
            hold on
            plot(pred_dates(where:where+14),pred.trueCases(where:where+14),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+14),pred.predsWW(where:where+14),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+14),pred.predsCase(where:where+14),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'YTick',[0 135000 270000])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+14)])
            xtickangle(0)
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Netherlands, July 2021 resurgence",'fontweight','normal','fontsize',12)
            ylim([0,3*max(pred.predsWW)])
        end
        
        function osh1(obj)
            k=9;       
            [pred, pred_dates] = load(obj,k);
            where = length(pred_dates)-12;
            
            hold on
            plot(pred_dates(where:where+12),pred.trueCases(where:where+12),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+12),pred.predsWW(where:where+12),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+12),pred.predsCase(where:where+12),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 75 150])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+12)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Oshkosh, July 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function ral1(obj)
            k=10;       
            [pred, pred_dates] = load(obj,k);
            where = length(pred_dates)-10;
            
            hold on
            plot(pred_dates(where:where+10),pred.trueCases(where:where+10),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsWW(where:where+10),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsCase(where:where+10),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            xtickangle(0)
            set(gca, 'YTick',[0 550 1100])
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+10)])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Raleigh, July 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function rie1(obj)
            k=11;       
            [pred, pred_dates] = load(obj,k);
            where = 10;
            
            hold on
            plot(pred_dates(where:where+11),pred.trueCases(where:where+11),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+11),pred.predsWW(where:where+11),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+11),pred.predsCase(where:where+11),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+11)])
            xtickangle(0)
            set(gca, 'YTick',[0 100 200])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Riera de la Bisbal, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function rie2(obj)
            k=11;       
            [pred, pred_dates] = load(obj,k);
            where = 20;
            
            hold on
            plot(pred_dates(where:where+8),pred.trueCases(where:where+8),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+8),pred.predsWW(where:where+8),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+8),pred.predsCase(where:where+8),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+8)])
            xtickangle(0)
            set(gca, 'YTick',[0 150 300])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Riera de la Bisbal, Jan 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function rie3(obj)
            k=11;       
            [pred, pred_dates] = load(obj,k);
            where = length(pred_dates)-8;
            
            hold on
            plot(pred_dates(where:where+8),pred.trueCases(where:where+8),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+8),pred.predsWW(where:where+8),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+8),pred.predsCase(where:where+8),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+8)])
            xtickangle(0)
            set(gca, 'YTick',[0 150 300])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Riera de la Bisbal, June 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function zur1(obj)
            k=12;       
            [pred, pred_dates] = load(obj,k);
            where = 1;
            
            hold on
            plot(pred_dates(where:where+10),pred.trueCases(where:where+10),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsWW(where:where+10),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+10),pred.predsCase(where:where+10),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+10)])
            xtickangle(0)
            set(gca, 'YTick',[0 2000 4000])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Zurich, Oct. 2020 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function zur2(obj)
            k=12;       
            [pred, pred_dates] = load(obj,k);
            where = 110;
            
            hold on
            plot(pred_dates(where:where+40),pred.trueCases(where:where+40),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+40),pred.predsWW(where:where+40),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+40),pred.predsCase(where:where+40),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+40)])
            xtickangle(0)
            set(gca, 'YTick',[200 600 1000])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Zurich, Mar. 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        function zur3(obj)
            k=12;       
            [pred, pred_dates] = load(obj,k);
            where = 220;
            
            hold on
            plot(pred_dates(where:where+30),pred.trueCases(where:where+30),'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+30),pred.predsWW(where:where+30),'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
            plot(pred_dates(where:where+30),pred.predsCase(where:where+30),'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
            datetick('x','dd mmm','keepticks', 'keeplimits')
            set(gca, 'XTick',[pred_dates(where) pred_dates(where+30)])
            xtickangle(0)
            set(gca, 'YTick',[0 2000 4000])
            ax=gca;
            ax.XAxis.FontSize = 12;
            ax.YAxis.FontSize = 11;
            grid on
            box on
            %title("Zurich, July 2021 resurgence",'fontweight','normal','fontsize',12)
        end
        
        
    end
    
end
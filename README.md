# CoWWAn

>__Note__
>
>The repository has moved here: [_gitlab.com/uniluxembourg/lcsb/systems-control/cowwan_](https://gitlab.com/uniluxembourg/lcsb/systems-control/cowwan). This repository instance will cease to exist in the future.


This is the code for the CoWWAn pipeline for Covid-19 monitoring by wastewater analysis. The approach is introduced in the article

__Daniele Proverbio, Francoise Kemp, Stefano Magni, Leslie Ogorzaly, Henry-Michel Cauchie, Jorge Goncalves, Alexander Skupin, and Atte Aalto__: "Model-based assessment of COVID-19 epidemic dynamics by wastewater analysis", _Science of the Total Environment_ 827, 154235, (2022), doi: [_https://doi.org/10.1016/j.scitotenv.2022.154235_](https://doi.org/10.1016/j.scitotenv.2022.154235), [_www.sciencedirect.com/science/article/pii/S0048969722013274_](https://www.sciencedirect.com/science/article/pii/S0048969722013274).

Please cite the article if you use the method in your work. The method is tested with Matlab 2019b.


## General information

The underlying model is based on the SEIR differential equation with an additional compartment for active cases (A in the figure) producing virions to wastewater. The model is coupled with the Extended Kalman filter for state estimation based on either case data (y<sub>c</sub>), wastewater data (y<sub>w</sub>), or both. The system's state consists of the sizes of each compartment, and the infectivity parameter (beta). This parameter lumps together social interaction levels, vaccination coverage, infectivity of viral variants, etc. and is therefore subject to changes over time.

<img src="data/CoWWAnPipeline.png" width="560">

## User guide

The data should be in a table (format xls, xlsx, or csv) with (at least) two  columns and exactly one line for each day. Missing data should be indicated by "-1". The data is given under the following column headers: 
- "_cases_" containing daily case numbers without any normalisation
- "_ww_" containing wastewater data

This refers to the way the data import is done. The method itself requires two vectors of equal length, one for non-normalised daily case numbers, and one for the wastewater data, where -1 denotes missing measurements.

Instructions on using the tool are detailed in the following sections. For details of the different files, see [here.](SEIRWWfiles/README.md)

### Model calibration and re-calibration

The model needs calibration when it is applied to a new region. Calibration is not needed every time new data arrives, but re-calibration can be done every now and then using either the whole data, or only part of it. Using only part of the data can be reasonable if there has been a significant change either in testing policy (although that can be accounted for through the `darkNumber` parameter) or in wastewater sampling protocol.


Model calibration is done using the file _initialise.m_. The user needs to set the population size, and the "dark number", which is the average ratio of total cases and detected cases. Such values can be obtained by seroprevalence studies. Different values can be used for different time periods. The `darkNumber` parameter should be a M x 2 matrix, where the first column contains the different ratios, and second column the start day of the corresponding value (given as the number of the day counted from the beginning of the data). For example, if only one value is used, it is given as

`params.darkNumber = [1.8 , 1];`

If one value is used for the beginning (say, ratio 3 for the first 90 days), and then a new value (due to increased testing activity, for example), it is given as

`params.darkNumber = [3 , 1; 1.8 91];`

The method estimates a weekly rhythm for tests, which is observed in many regions due to reduced testing on weekends (see Note 2). This estimation results in daily coefficients c<sub>t</sub> that indicate the daily share of detected cases out of all cases (vector `C` in the code). The c<sub>t</sub> values are normalised such that their weekly average is the inverse of the `darkNumber` parameter. In summary, `darkNumber` gives the average ratio of total and detected cases over a longer period, whereas c<sub>t</sub> accounts for the finer-grained fluctuations. In addition to the weekly rhythm, non-weekend holidays might cause reduced case numbers that cannot be anticipated by the method. The indices of such special holidays are given to the method through the `specialHolidays` variable. 

>__Note 1__
>
>The model projections are somewhat sensitive to the `darkNumber` parameter, as shown in the sensitivity analysis of our manuscript. However, there we only considered changing one parameter at a time. Re-calibration reduces the sensitivity. In general, one should not be too concerned if only a crude estimate is available. However, in many regions this ratio has decreased considerably after the initial phase of the pandemic. We advice to include such changes, since that has an effect on the model calibration. 

>__Note 2__
>
>If the "weekend effect" is irregular, manual intervention might be needed for setting the c<sub>t</sub> values. This can be done by simply overwriting the `C` vector given by the _SEIRWWinit_ function, by `C = ones(size(C))/darkNumber;` and then for days with reduced testing, use for example `C(131) = 0.3*C(131);`.

### Running the method

The file _runMethod.m_ contains the scripts for the basic use of the method. The script can be run section by section, after running the initial section where data and parameters are loaded (that have been calibrated earlier). The file contains sections for estimating the wastewater data using only case data, and for estimating the case data using only wastewater data either without interpolation or with interpolation. The next sections are used for estimating the effective reproduction number R<sub>eff</sub>(t) and for projections forward in time. 


>__Note 3__
>
>The future projections are done by keeping the beta(t)-parameter unchanged. The uncertainty in the projections is only due to the uncertainty in the estimate of the current parameter. Therefore uncertainty related to possible future changes in the parameter are not accounted for by the projections. The error estimates do not account for the noise in the model either and therefore the uncertainty is underestimated for the short-term projections.


## Manuscript result reproduction

Codes for reproducing the results and plots of the article can be found in a separate folder _seir-ww/articleReproduction/_. Data import and model calibration is done in the file _mainInit.m_. The analysis is done in the file _main.m_ where the user needs to choose the considered region in the first code section. The next sections can be run one-by-one. Running these sections generates the plots for Supplementary Figs. 3-14, saves the results to be used for other plots, and displays in the command window the standardised and normalised projection errors of Supplementary table 2, as well as the correlations shown in Fig. 1d. The last section does the longer-term projections and plots for Luxembourg, shown in Supplementary Fig. 15.

Sensitivity analysis (Supplementary Fig. 18) is done in the file _sensitivityAnalysis.m_ and the experiment on the projection horizon length (Fig. 2c) in the file _winLengthExp.m_. These need to be run before plotting.

The codes for generating other plots are located in the folder _seir-ww/articleReproduction/Figs_src/_ where the files _Figs_main.m_ and _Figs_supMat.m_ contain all necessary information.




